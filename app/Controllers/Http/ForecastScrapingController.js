'use strict'

class ForecastScrapingController {
  async process({ response }) {
    try {
      const cheerio = require('cheerio');
      const moment = require('moment');
      const axios = require('axios');
      //Get de Urls's
      let urlCities = await axios.get("http://127.0.0.1:3333/cities");
      let arrCities = urlCities.data;
      let forecast = []
      for(let i=0; i<arrCities.length; i++){
        let page = await axios.get(arrCities[i]["url"]);
        let $ = cheerio.load(page.data);
        $("a.forecast-card").each((index,element)=>{
          let date_forecast = {
            "date" : moment($(element).find('div.date > p.sub').text().trim()+"/"+moment().year(),"MM/DD/YYYY").format("YYYY-MM-DD"),
            "temphigh" : $(element).find('div.temps > span.high').text().trim().replace("°", ""),
            "templow" : $(element).find('div.temps > span.low').text().trim().replace(/°| |\//gi, (x) => ""),
            "phrase" : $(element).find('span.phrase').text().trim(),
            "precip" : $(element).find('div.precip > p:last-child').text().trim().replace("%", ""),
            "cit_id": arrCities[i]["id"]
          }
          forecast.push(date_forecast)
        })
      }
      const attributes = {
        "forecast":forecast
      }      
      await axios.post("http://127.0.0.1:3333/forecastMasivo",attributes);
      return response.removeHeader('Set-Cookie', 'Server').status(200).json(forecast)
    } catch (err) {
        return response.removeHeader('Set-Cookie', 'Server').status(500).json(err)
    }
  }  
}

module.exports = ForecastScrapingController
